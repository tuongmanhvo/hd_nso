﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace HD_NSO.Interfaces
{
    public interface IErrorHandleService
    {
        void SendDetailError(Exception ex);
    }
}
