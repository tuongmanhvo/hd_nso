﻿namespace HD_NSO.Interfaces
{
    public interface IProcess
    {
        void Execute();
    }
}
