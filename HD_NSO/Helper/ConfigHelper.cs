﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HD_NSO.Helper
{
    public static class ConfigHelper
    {
        public static string GetAppSetting(string key)
        { 
            return ConfigurationManager.AppSettings[key];
        }

        public static string GetConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }

        public static string GetDefaultConnectionString()
        {
            return GetConnectionString("BNIConnectionString");
        }
    }
}
