﻿using Autofac;
using HD_NSO.Interfaces;
using HD_NSO.Services;

namespace HD_NSO.Ioc
{
    public static class ContainerConfigure
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<SmtpEmailService>().As<IEmailService>();
            builder.RegisterType<HD_NSO_EmailProcess>().As<IProcess>();
            builder.RegisterType<ErrorHandleService>().As<IErrorHandleService>();

            return builder.Build();
        }
    }
}
