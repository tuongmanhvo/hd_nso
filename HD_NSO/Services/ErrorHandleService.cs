﻿using System;
using HD_NSO.Helper;
using HD_NSO.Interfaces;

namespace HD_NSO.Services
{
    public class ErrorHandleService : IErrorHandleService
    {
        private readonly IEmailService _emailService;
        
        public ErrorHandleService(IEmailService emailService)
        {
            _emailService = emailService;
        }

        public void SendDetailError(Exception ex)
        {
            var message = EmailHelper.BuildErrorMessage(ex);
            _emailService.Send(message);
        }
    }
}
