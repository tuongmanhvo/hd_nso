﻿using HD_NSO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace HD_NSO.Services
{
    public interface IEmailService
    {
        void Send(MailMessage message);
    }
}
