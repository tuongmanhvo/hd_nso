﻿namespace HD_NSO.Models
{
    public class EmailTemplate
    {
        public string EmailType { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
