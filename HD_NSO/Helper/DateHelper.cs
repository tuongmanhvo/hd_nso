﻿using System;

namespace HD_NSO.Helper
{
    public static class DateHelper
    {
        public static string FormatDateAsString(DateTime date)
        {
            return date.ToString(ConfigHelper.GetAppSetting("DateFormat"));
        }
    }
}
