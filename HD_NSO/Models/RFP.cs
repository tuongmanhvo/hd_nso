﻿using HD_NSO.Constants;
using HD_NSO.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HD_NSO.Models
{
    public class HD_NSO_Report
    {
        public int JobID { get; set; }
        public int? CreatedLeadID { get; set; }
        public string SPR { get; set; }
        public string PostAs { get; set; }
        public DateTime? LeadReceived { get; set; }
        public DateTime? CreatedDate { get; set; }

        public string DateRecieved { get; set; }
        public string TimeRecieved { get; set; }
        public string DateSubmit{ get; set; }
        public string TimeSubmit { get; set; }
        public string TimeGap { get; set; }

    }
}
