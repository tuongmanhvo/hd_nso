﻿using System;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using HD_NSO.Models;

namespace HD_NSO.Helper
{
    public static class EmailHelper
    {
        public static MailMessage BuildMailMessage(EmailTemplate template, MemoryStream stream)
        {
            var message = new MailMessage();

            message.From = GetFrom();
            message.To.Add(GetTo());
            message.Subject = template.Subject;// RenderTemplate(template.Subject, data);
            message.Body = string.Empty;//RenderTemplate(template.Body, rfp);
            message.IsBodyHtml = true;

            stream.Position = 0;
            var contentType = new ContentType(MediaTypeNames.Application.Octet);
            var attach = new Attachment(stream, contentType);
            attach.ContentDisposition.FileName = string.Format("{0:H_mm_ss} – NSO Webform.xlsx", DateTime.Now);
            //attach.ContentDisposition.Size = attach.Length;
            message.Attachments.Add(attach); //"Time Stamp – NSO Webform.xlsx"

            //message.Attachments.Add(new Attachment(stream, string.Format("{0:hh:mm:ss} – NSO Webform.xlsx",DateTime.Now), contentType)); //"Time Stamp – NSO Webform.xlsx"
            return message;
        }

        public static MailMessage BuildErrorMessage(Exception ex)
        {
            var message = new MailMessage();

            message.From = new MailAddress(ConfigHelper.GetAppSetting("Sender"));
            message.To.Add(CleanEmailAddress(ConfigHelper.GetAppSetting("WhoGetsErrorEmails")));
            message.Subject = string.Format("[OptIn-Automated Emails] Error on {0}", DateTime.Now.ToString());
            message.Body = string.Format("Source: {0}<br><br>Message: {1}<br><br>TrackTrace: {2}", ex.Source, ex.Message, ex.StackTrace);
            message.IsBodyHtml = true;

            return message;
        }

      
        private static MailAddress GetFrom()
        {
            return new MailAddress(ConfigHelper.GetAppSetting("Sender"));
        }

        private static string GetTo()
        {
            return ConfigHelper.GetAppSetting("Environment").Equals("PROD",StringComparison.Ordinal) ?
                CleanEmailAddress(ConfigHelper.GetAppSetting("WhoGetsEmails")) :
                CleanEmailAddress(ConfigHelper.GetAppSetting("WhoGetsTestingEmails"));
        }

    
        private static string CleanEmailAddress(string emailAddress)
        { 
            return string.IsNullOrEmpty(emailAddress) ? string.Empty : emailAddress.Replace(';', ',');
        }
    }
}
