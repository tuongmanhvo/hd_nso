﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HD_NSO.Constants
{
    public static class SqlView
    {
        public const string VW_OptInRFPsForInitialEmail = "vwOptInRFPsForInitialEmail";
        public const string VW_OptInRFPsFor1stFollowupEmail = "vwOptInRFPsFor1stFollowupEmail";
        public const string VW_OptInRFPsFor2ndFollowupEmail = "vwOptInRFPsFor2ndFollowupEmail";
        public const string VW_OptInRFPsForOneOffTimedEmail = "vwOptInRFPsForOneOffTimedEmail";
        public const string VW_EmailTemplates = "HD_OptInAutomation_Ref_EmailTemplates";
    }
}
