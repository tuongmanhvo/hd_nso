﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using log4net;
using System.Reflection;
using HD_NSO.Models;

namespace HD_NSO.Helper
{
    public static class RFPHelper
    {
        private static readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private const string DateFormat = "MM/dd/yyyy";
        private const string TimeFormat = "hh:mm tt";
        public static IEnumerable<HD_NSO_Report> GetDataReport()
        {
            IEnumerable<HD_NSO_Report> result;

            try
            {
                using (var conn = new SqlConnection(ConfigHelper.GetDefaultConnectionString()))
                {
                    conn.Open();
                    var sql = string.Format(
                        "select [JobID] ,[CreatedLeadID],[SPR],[PostAs],[LeadReceived],[CreatedDate] FROM [BNI].[dbo].[HD_DirectLead] as l join [BNI].[dbo].[HiltonDirect_Form] as f on l.[JobID]=f.DirectLeadID where f.FormType='NSO' and {0} ",
                        BuildQueryConditions());
                    result = conn.Query<HD_NSO_Report>(sql, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Error at GetDataReport. Detail: {0}", ex);
                throw;
            }

            return result;
        }

        private static string BuildQueryConditions()
        {
            var lastDate = DateTime.Today.AddDays(-1);
            var startDate = new DateTime(lastDate.Year, lastDate.Month, 1);
            return string.Format(" [CreatedDate]>='{0:yyyy/MM/dd}' and [CreatedDate] <'{1:yyyy/MM/dd}' ", startDate, lastDate.AddDays(1));// db contain time, be carefull to compare dates only
        }

        public static void ProcessData(IEnumerable<HD_NSO_Report> rows)
        {

            foreach (var row in rows)
            {
                if (row.LeadReceived.HasValue)
                {
                    row.DateRecieved = row.LeadReceived.Value.ToString(DateFormat);
                    row.TimeRecieved = row.LeadReceived.Value.ToString(TimeFormat);
                }

                if (row.CreatedDate.HasValue)
                {
                    row.DateSubmit = row.CreatedDate.Value.ToString(DateFormat);
                    row.TimeSubmit = row.CreatedDate.Value.ToString(TimeFormat);
                }

                if (row.LeadReceived.HasValue && row.CreatedDate.HasValue)
                {
                    var span = row.CreatedDate.Value - row.LeadReceived.Value;
                    row.TimeGap = Math.Floor(Math.Abs(span.TotalMinutes)).ToString();
                }
            }
        }

    }
}
