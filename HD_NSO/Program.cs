﻿using Autofac;
using HD_NSO.Interfaces;
using HD_NSO.Ioc;
using log4net.Config;

namespace HD_NSO
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            var container = ContainerConfigure.Configure();

            using (var scope = container.BeginLifetimeScope())
            {
                var mainProcess = container.Resolve<IProcess>();
                mainProcess.Execute();
            }
        }
    }
}
