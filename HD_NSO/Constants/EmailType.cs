﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HD_NSO.Constants
{
    public static class EmailType
    {
        public const string Initial = "Initial";
        public const string FirstFollowup = "FirstFollowup";
        public const string SecondFollowup = "SecondFollowup";
        public const string OneOff = "OneOff";
    }
}
