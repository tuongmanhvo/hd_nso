﻿using log4net;
using System;
using System.Net.Mail;
using System.Reflection;
using HD_NSO.Helper;

namespace HD_NSO.Services
{
    public class SmtpEmailService : IEmailService
    {
        private static readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void Send(MailMessage message)
        {
            try
            {
                using (var smtp = new SmtpClient())
                {
                    smtp.Host = ConfigHelper.GetAppSetting("EmailServer");
                    //smtp.Port = 25;
                    //smtp.EnableSsl = true;
                    //smtp.UseDefaultCredentials = true;
                    //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {
                _logger.DebugFormat("Cannot send email due to {0}", ex);
                throw ex;
            }
        }
    }
}
