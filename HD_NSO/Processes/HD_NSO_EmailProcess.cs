﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using DocumentFormat.OpenXml.Spreadsheet;
using HD_NSO.Helper;
using HD_NSO.Interfaces;
using HD_NSO.Models;
using SpreadsheetLight;

namespace HD_NSO.Services
{
    public class HD_NSO_EmailProcess : IProcess
    {
        private static readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static string dirPath = ConfigHelper.GetAppSetting("ExcelDirPath");
        private const int STARTING_ROW = 2;
        private const string COLUMN_JOBID = "A";
        private const string COLUMN_LEADID = "B";
        private const string COLUMN_SPR = "C";
        private const string COLUMN_POSTAS = "D";
        private const string COLUMN_DATERECEIVED = "E";
        private const string COLUMN_TIMERECEIVED = "F";
        private const string COLUMN_DATESUBMIT = "G";
        private const string COLUMN_TIMESUBMIT = "H";
        private const string COLUMN_TIMEGAP = "I";

        private readonly IEmailService _emailService;
        private readonly IErrorHandleService _errorHandleService;

        private IEnumerable<EmailTemplate> emailTemplates;
        public HD_NSO_EmailProcess(IEmailService emailService, IErrorHandleService errorHandleService)
        {
            this._emailService = emailService;
            this._errorHandleService = errorHandleService;
        }

        public void Execute()
        {
            try
            {
                DateTime start, end;

                start = DateTime.Now;
                _logger.InfoFormat("Process started at {0}", start);

                SendEmail();

                end = DateTime.Now;
                _logger.InfoFormat("Process ended at {0}", end);

            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Error at Execute. Trying to send email with detail error. Detail: {0}", ex);

                try
                {
                    _errorHandleService.SendDetailError(ex);
                }
                catch (Exception ex1)
                {
                    _logger.ErrorFormat("Error at sending email. Detail: {0}", ex1);
                }
            }
        }

        private void SendEmail()
        {
            try
            {
                var rows = RFPHelper.GetDataReport();
                var hdNsoReports = rows as HD_NSO_Report[] ?? rows.ToArray();
                RFPHelper.ProcessData(hdNsoReports);
                MemoryStream stream = new MemoryStream();
                ExportToFile(stream, hdNsoReports);

                var emailTemplate = new EmailTemplate
                {
                    Body = string.Empty,
                    Subject = string.Format("NSO {0:MM-dd-yyyy/H:mm:ss} Report", DateTime.Now)
                };
                _logger.InfoFormat("Begin sending emails");

                var mail = EmailHelper.BuildMailMessage(emailTemplate, stream);

                _logger.DebugFormat("Sending email");
                _emailService.Send(mail);

                _logger.InfoFormat("Completed sending emails");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void ExportToFile(MemoryStream stream, IEnumerable<HD_NSO_Report> rows)
        {
            var templatePath = Path.Combine(dirPath, "HD_NSO_Report.xlsx");
            SLDocument sl = new SLDocument(templatePath, "Export");
            SLStyle styleResponseDate = sl.CreateStyle();
            styleResponseDate.Fill.SetPattern(PatternValues.Solid, SLThemeColorIndexValues.Accent5Color, SLThemeColorIndexValues.Accent5Color);

            int currentRow = STARTING_ROW;
            foreach (var row in rows)
            {
                sl.CopyRow(STARTING_ROW, currentRow);

                sl.SetCellValue(string.Format("{0}{1}", COLUMN_JOBID, currentRow), row.JobID);
                sl.SetCellValue(string.Format("{0}{1}", COLUMN_LEADID, currentRow), row.CreatedLeadID.HasValue? row.CreatedLeadID.ToString(): string.Empty);
                sl.SetCellValue(string.Format("{0}{1}", COLUMN_SPR, currentRow), row.SPR);
                sl.SetCellValue(string.Format("{0}{1}", COLUMN_POSTAS, currentRow), row.PostAs);
                sl.SetCellValue(string.Format("{0}{1}", COLUMN_DATERECEIVED, currentRow), row.DateRecieved);
                sl.SetCellValue(string.Format("{0}{1}", COLUMN_TIMERECEIVED, currentRow), row.TimeRecieved);
                sl.SetCellValue(string.Format("{0}{1}", COLUMN_DATESUBMIT, currentRow), row.DateSubmit);
                sl.SetCellValue(string.Format("{0}{1}", COLUMN_TIMESUBMIT, currentRow), row.TimeSubmit);
                sl.SetCellValueNumeric(string.Format("{0}{1}", COLUMN_TIMEGAP, currentRow), row.TimeGap);

                currentRow++;
            }
            //SLStyle styleWhite = sl.CreateStyle();
            //styleWhite.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.White, System.Drawing.Color.White);
            //styleWhite.FormatCode = "#,##0";
            //sl.SetColumnStyle(9, 9, styleWhite);

            //sl.SaveAs("test.xlsx");//(stream);
            sl.SaveAs(stream);
        }

    }
}
